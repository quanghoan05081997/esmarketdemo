package vn.com.engma.esmarketDemo1.data;


import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import vn.com.engma.esmarketDemo1.data.local.LocalRepository;
import vn.com.engma.esmarketDemo1.data.remote.RemoteRepository;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.NewsModel;
import vn.com.engma.esmarketDemo1.data.remote.dto.User;


/**
 * Created by AhmedEltaher on 5/12/2016
 */

public class DataRepository implements DataSource {
    private RemoteRepository remoteRepository;
    private LocalRepository localRepository;


    @Inject
    public DataRepository(RemoteRepository remoteRepository, LocalRepository localRepository) {
        this.remoteRepository = remoteRepository;
        this.localRepository = localRepository;
    }

    @Override
    public Single<NewsModel> requestNews() {
        return remoteRepository.getNews();
    }

    @Override
    public Single<ModelCagetory> requestGetCategory(Map<String, String> map) {
        return remoteRepository.getRecordsPerCategory(map);
    }


    @Override
    public Single<User.UserRespond> requestLogin(String username, String password) {
        return remoteRepository.login(username, password);
    }

}
