package vn.com.engma.esmarketDemo1.utils;

/**
 * Created by AhmedEltaher on 5/12/2016
 */

public class Constants {
    public static final String LANG_EN = "en";
    public static final String LANG_VI = "vi";


    public static final String PARAM_RESULT_CODE = "PARAM_RESULT_CODE";
    public static final String PARAM_ERROR_MESSAGE = "PARAM_ERROR_MESSAGE";
    public static final String PARAM_SERVICE_CODE = "PARAM_SERVICE_CODE";


    //bundle key
    public static final String BUNDLE_SETTING_LANGUAGE = "BUNDLE_SETTING_LANGUAGE";
    //service code
    public static final int AIR_CONDITIONER_SERVICE_CODE = 0;
    public static final int CLEANUP_SERVICE_CODE = 1;
    public static final int CABLE_SERVICE_CODE = 2;
    public static final String PARAM_SERVICE_ID = "PARAM_SERVICE_ID";
    public static final String EXTRA_IS_WAITING_SERVICE = "EXTRA_IS_WAITING_SERVICE";
    public static final String EXTRA_IS_MY_QUESTION = "EXTRA_IS_MY_QUESTION";
    public static final String EXTRA_QUESTION_ID = "EXTRA_QUESTION_ID";

    public static final String PHONE_VERSION = "ANDROID ";
    public static final String LIMIT_RECORD_DEFAULT = "6";
    
    public static final String OFFSET_DEFAULT = "0";
    public static final String LIMIT_DEFAULT = "10";
    public static final String IS_FEATURE_DEFAULT = "TRUE" ;
    //splash activity
    public static int SPLASH_DELAY = 1800;

    public static int ERROR_UNDEFINED = -1;
    //    public static String BASE_URL = "https://api.nytimes.com/svc/";
    //check valid email
    public static final int MAX_LENGTH_EMAIL = 50;
    public static final int MIN_LENGTH_EMAIL = 11;
}
