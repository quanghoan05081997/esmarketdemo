package vn.com.engma.esmarketDemo1.data.social;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import vn.com.engma.esmarketDemo1.utils.L;


/**
 * Created by nhanmai on 1/16/18.
 */

public class GooglePlusAPI implements GoogleApiClient.OnConnectionFailedListener {

    private LoginSocialResult loginSocialResult;

    public static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private AppCompatActivity activity;


    public GooglePlusAPI(AppCompatActivity activity, LoginSocialResult listener) {

        this.activity = activity;
        this.loginSocialResult = listener;

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
    }

    public void signIn() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                            activity.startActivityForResult(signInIntent, RC_SIGN_IN);
                        }
                    });
        }

    }


    public void handleSignInResult(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        handleSignInResult(result);
    }

    public void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess() && result.getSignInAccount() != null) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Uri urlImgSrc = acct.getPhotoUrl();
            String email = acct.getEmail() != null ? acct.getEmail().trim() : "";
            String first_name = acct.getFamilyName() != null ? acct.getFamilyName().trim() : "";
            String last_name = acct.getGivenName() != null ? acct.getGivenName().trim() : "";
            loginSocialResult.loginResult(LoginType.GG, acct.getId(), email, "", first_name, last_name, "", urlImgSrc != null ? urlImgSrc.toString() : "");
            L.e("Nhan", "handleSignInResult");

        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
