package vn.com.engma.esmarketDemo1.ui.base.listeners;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface ErrorView {
    void showError(String errorMessage);
}
