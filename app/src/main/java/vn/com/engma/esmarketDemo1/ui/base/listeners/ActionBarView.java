package vn.com.engma.esmarketDemo1.ui.base.listeners;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface ActionBarView {

    void setUpIconVisibility(boolean visible);

    void setTitle(String titleKey);

    void setSettingsIconVisibility(boolean visibility);

    void showNavigationMenu(boolean visibility);

    void showBackIcon(boolean visibility);
}
