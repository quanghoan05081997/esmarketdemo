
package vn.com.engma.esmarketDemo1.data.remote.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class NotificationItem implements Parcelable {

    @SerializedName("notification_id")
    private String question_id;

    @SerializedName("avatar_src")
    private String avatar_src;

    @SerializedName("user_name")
    private String user_name;

    @SerializedName("date")
    private String date;

    @SerializedName("question")
    private String question;

    @SerializedName("isUnread")
    private int isUnread;

    @SerializedName("isBuilderNotif")
    private int isBuilderNotif;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.question_id);
        dest.writeString(this.avatar_src);
        dest.writeString(this.user_name);
        dest.writeString(this.date);
        dest.writeString(this.question);
        dest.writeInt(this.isUnread);
        dest.writeInt(this.isBuilderNotif);
    }

    public NotificationItem() {
    }

    protected NotificationItem(Parcel in) {
        this.question_id = in.readString();
        this.avatar_src = in.readString();
        this.user_name = in.readString();
        this.date = in.readString();
        this.question = in.readString();
        this.isUnread = in.readInt();
        this.isBuilderNotif = in.readInt();
    }

    public static final Creator<NotificationItem> CREATOR = new Creator<NotificationItem>() {
        @Override
        public NotificationItem createFromParcel(Parcel source) {
            return new NotificationItem(source);
        }

        @Override
        public NotificationItem[] newArray(int size) {
            return new NotificationItem[size];
        }
    };

    public String getAvatar_src() {
        return avatar_src;
    }

    public void setAvatar_src(String avatar_src) {
        this.avatar_src = avatar_src;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuestion() {
        return question;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getIsUnread() {
        return isUnread;
    }

    public int getIsBuilderNotif() {
        return isBuilderNotif;
    }

    public void setIsBuilderNotif(int isBuilderNotif) {
        this.isBuilderNotif = isBuilderNotif;
    }

    public void setIsUnread(int isUnread) {
        this.isUnread = isUnread;
    }

    public static Creator<NotificationItem> getCREATOR() {
        return CREATOR;
    }
}
