package vn.com.engma.esmarketDemo1.data;

import java.util.Map;

import io.reactivex.Single;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.NewsModel;
import vn.com.engma.esmarketDemo1.data.remote.dto.User;

/**
 * Created by ahmedeltaher on 3/23/17.
 */

interface DataSource {
    Single<NewsModel> requestNews();
    Single<ModelCagetory> requestGetCategory(Map<String,String> map);
    Single<User.UserRespond> requestLogin(String username, String password);
}
