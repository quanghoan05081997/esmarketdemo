package vn.com.engma.esmarketDemo1.ui.component.fragment.home_fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.CagetoryItem;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.ProductItem;

/**
 * Created by pc-computer on 5/17/2018.
 */

public class AdapterHome extends BaseAdapter {

    private Context context;
    LayoutInflater layoutInflater;
    private ModelCagetory categoryModel;


    public AdapterHome(Context context, ModelCagetory categoryModel) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.categoryModel = categoryModel;
    }

    @Override
    public int getCount() {
        return categoryModel.getProductItemList().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    class ViewHolder {
        TextView title;
        TextView price1;
        TextView price2;
        TextView price3;
        TextView salePrice1;
        TextView salePrice2;
        TextView salePrice3;
        ImageView imgProduct1;
        ImageView imgProduct2;
        ImageView imgProduct3;
        LinearLayout lnLayout1;
        LinearLayout lnCategory;
        LinearLayout ln3;
        LinearLayout ln4;
    }




    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        CagetoryItem cagetoryItem = categoryModel.getProductItemList().get(i);

        if (view==null){

            view = layoutInflater.inflate(R.layout.category_home_s06, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.txtloaisanpham);
            viewHolder.lnLayout1 = (LinearLayout) view.findViewById(R.id.lnLayout1);
            viewHolder.lnCategory = (LinearLayout) view.findViewById(R.id.lnCategory);
            viewHolder.ln3 = (LinearLayout) view.findViewById(R.id.ln3);
            viewHolder.ln4 = (LinearLayout) view.findViewById(R.id.ln4);

            viewHolder.imgProduct1 = (ImageView) view.findViewById(R.id.imgProduct1);
            viewHolder.imgProduct2 = (ImageView) view.findViewById(R.id.imgProduct2);
            viewHolder.imgProduct3 = (ImageView) view.findViewById(R.id.imgProduct3);

            viewHolder.price1 = (TextView) view.findViewById(R.id.tvPrice);
            viewHolder.price2 = (TextView) view.findViewById(R.id.tvPrice2);
            viewHolder.price3 = (TextView) view.findViewById(R.id.tvPrice3);

            viewHolder.salePrice1 = (TextView) view.findViewById(R.id.tvSalePrice);
            viewHolder.salePrice2 = (TextView) view.findViewById(R.id.tvSalePrice2);
            viewHolder.salePrice3 = (TextView) view.findViewById(R.id.tvSalePrice3);

            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.title.setText(cagetoryItem.getName());
        for (i = 0; i < cagetoryItem.getProductItemList().size(); i++) {

            ProductItem productItem;
            productItem = cagetoryItem.getProductItemList().get(i);

            if (i == 0){
                Picasso.with(context).load(productItem.getImg_src()).into(viewHolder.imgProduct1);
                viewHolder.price1.setText(productItem.getPrice()+"đ");
                viewHolder.salePrice1.setText((productItem.getSale_price()+"đ"));
            }else if (i == 1)
            {
                Picasso.with(context).load(productItem.getImg_src()).into(viewHolder.imgProduct2);
                viewHolder.price2.setText(productItem.getPrice()+"đ");
                viewHolder.salePrice2.setText((productItem.getSale_price()+"đ"));

            }
            else if (i == 2){
                Picasso.with(context).load(productItem.getImg_src()).into(viewHolder.imgProduct3);
                viewHolder.price3.setText(productItem.getPrice()+"đ");
                viewHolder.salePrice3.setText((productItem.getSale_price()+"đ"));
            }

        }
        return view;
    }
}
