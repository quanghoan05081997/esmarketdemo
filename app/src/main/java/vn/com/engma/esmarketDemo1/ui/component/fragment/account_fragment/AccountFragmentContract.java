package vn.com.engma.esmarketDemo1.ui.component.fragment.account_fragment;

import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;


/**
 * Created by tuannguyen on 13/12/17.
 */

public interface AccountFragmentContract {
    interface View extends BaseView {

    }

    interface Presenter {
    }
}
