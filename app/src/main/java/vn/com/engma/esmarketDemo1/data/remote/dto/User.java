package vn.com.engma.esmarketDemo1.data.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import vn.com.engma.esmarketDemo1.data.remote.dto.base.EObject;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;

/**
 * Created by nhanmai on 1/15/18.
 */

public class User extends EObject {
    public static class UserRespond extends ERespond {
        User data;

        public User getData() {
            return data;
        }
    }

    @Expose
    @SerializedName("email")
    String email;

    @Expose
    @SerializedName("fullname")
    String fullname;

    @Expose
    @SerializedName("phone")
    String phone;

    @Expose
    @SerializedName("birthday")
    String birthday;


    @Expose
    @SerializedName("gender")
    String gender;

    @Expose
    @SerializedName("crop_img_src")
    String crop_img_src;

    @Expose
    @SerializedName("img_src")
    String img_src;

    @Expose
    @SerializedName("language_code")
    String language_code;

    @Expose
    @SerializedName("social_type")
    String social_type;


    @Expose
    @SerializedName("jwt")
    String jwt;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCrop_img_src() {
        return crop_img_src;
    }

    public void setCrop_img_src(String crop_img_src) {
        this.crop_img_src = crop_img_src;
    }

    public String getImg_src() {
        return img_src;
    }

    public void setImg_src(String img_src) {
        this.img_src = img_src;
    }

    public String getLanguage_code() {
        return language_code;
    }

    public void setLanguage_code(String language_code) {
        this.language_code = language_code;
    }

    public String getSocial_type() {
        return social_type;
    }

    public void setSocial_type(String social_type) {
        this.social_type = social_type;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
