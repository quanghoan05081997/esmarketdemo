package vn.com.engma.esmarketDemo1.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import vn.com.engma.esmarketDemo1.BuildConfig;
import vn.com.engma.esmarketDemo1.R;


/**
 * Created by Dev.TuanNguyen on 12/16/2017.
 */

public class CommonUtils {

    public static void showAppToast(final Activity activity, final int resId) {
        if (activity != null && activity.getString(resId) != null) {
            showAppToast(activity, activity.getString(resId));
        }
    }

    public static void showAppToast(final Activity activity, String message) {
        if (activity != null && !TextUtils.isEmpty(message)) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
            });

        }
    }


    public static String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return Constants.PHONE_VERSION + sdkVersion + " (" + release + ")";
    }

    public static String getDeviceModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static Map<String, String> sendIdPhone(Map<String, String> params, Context context) throws JSONException {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String push_firebase_token = FirebaseInstanceId.getInstance().getToken();
        params.put(ParamsConstants.PUSH_FIREBASE_TOKEN, push_firebase_token);
        if (tManager != null) {
            String uid = push_firebase_token;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                uid = tManager.getDeviceId();
            } else {
                String deviceId = tManager.getDeviceId();
                if (!TextUtils.isEmpty(deviceId)) {
                    uid = deviceId;
                }
            }
            params.put(ParamsConstants.PARAM_DEVICE_ID, uid);
            params.put(ParamsConstants.PARAM_PHONE_NAME, Constants.PHONE_VERSION + getDeviceModel());
            params.put(ParamsConstants.PARAM_PHONE_VERSION, getAndroidVersion());

            params.put(ParamsConstants.PARAM_APPLICATION_ID, BuildConfig.APPLICATION_ID);
            params.put(ParamsConstants.PARAM_APP_VERSION, String.valueOf(BuildConfig.VERSION_CODE));
        }
        return params;
    }

    public static String hashPassword(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return SecurityUtils.hashSHA256(password);
    }

//    public static HashMap<String, Integer> isValidEmail(String email) {
//        HashMap<String, Integer> resultData = new HashMap<>();
//        if (TextUtils.isEmpty(email)) {
//            resultData.put(Constants.PARAM_RESULT_CODE, 0);
//            resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_empty_email);
//        } else {
//            if (isEmailValid(email)) {
//                resultData.put(Constants.PARAM_RESULT_CODE, 1);
//            } else {
//                resultData.put(Constants.PARAM_RESULT_CODE, 0);
//                resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_valid_email);
//            }
//        }
//        return resultData;
//    }

    public static void hideKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static boolean isUsernameValid(String username) {
        boolean isvalid = false;
        String expression = "^[a-zA-Z][a-zA-Z0-9._-]{3,49}$"; //first character must be alphabet and String must be 3 characters at least
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        CharSequence inputStr = username;
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches() && username.length() >= 3) {
            isvalid = true;
        }
        return isvalid;
    }

    public static boolean isPasswordValid(String password) {
        boolean isValid = true;
        if (password.length() < 6 || password.indexOf(' ') > -1) {
            return false;
        }
        return isValid;
    }

    public static boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPhoneValid(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }

    public static HashMap<String, Integer> isUserNameValid(String username) {
        HashMap<String, Integer> resultData = new HashMap<>();
        if (TextUtils.isEmpty(username)) {
            resultData.put(Constants.PARAM_RESULT_CODE, 0);
            resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_empty_username);
        } else {
            if (isUsernameValid(username)) {
                resultData.put(Constants.PARAM_RESULT_CODE, 1);
            } else {
                if (username.length() >= 3) {
                    resultData.put(Constants.PARAM_RESULT_CODE, 0);
                    resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_space_character);
                } else {
                    resultData.put(Constants.PARAM_RESULT_CODE, 0);
                    resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_invalid_username);
                }

            }
        }
        return resultData;
    }

    public static HashMap<String, Integer> isValidPassword(String password) {
        HashMap<String, Integer> resultData = new HashMap<>();
        if (TextUtils.isEmpty(password)) {
            resultData.put(Constants.PARAM_RESULT_CODE, 0);
            resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_empty_password);
        } else {
            if (isPasswordValid(password)) {
                resultData.put(Constants.PARAM_RESULT_CODE, 1);
            } else {
                resultData.put(Constants.PARAM_RESULT_CODE, 0);
                resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_invalid_password);
            }
        }
        return resultData;
    }

    public static HashMap<String, Integer> isValidFullName(String fullName) {
        HashMap<String, Integer> resultData = new HashMap<>();
        if (TextUtils.isEmpty(fullName)) {
            resultData.put(Constants.PARAM_RESULT_CODE, 0);
            resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_empty_fullname);
        } else {
            if (isValidFullname(fullName)) {
                resultData.put(Constants.PARAM_RESULT_CODE, 1);
            } else {
                resultData.put(Constants.PARAM_RESULT_CODE, 0);
                resultData.put(Constants.PARAM_ERROR_MESSAGE, R.string.err_invalid_fullname);
            }
        }
        return resultData;
    }

    private static boolean isValidFullname(String fullName) {
        boolean isValid = true;
        if (fullName.length() < 4) {
            return false;
        }
        return isValid;
    }

    public static void setLanguage(Context context, String language) {

        Locale myLocale = new Locale(language);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
