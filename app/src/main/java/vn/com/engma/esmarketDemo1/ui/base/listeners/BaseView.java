package vn.com.engma.esmarketDemo1.ui.base.listeners;

import android.view.View;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface BaseView {
    void navigateToHomeScreen();

    void showDialog();

    void dismissDialog();

    void showErrToast(int errMessage);
    void showErrorMessage(int errMessage);
    void showPopupMenu();

    void showToast(int errMessage);

    void showPopupMenu(View view);


}
