package vn.com.engma.esmarketDemo1.ui.component.fragment.home_fragment;

import android.app.Activity;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;
import vn.com.engma.esmarketDemo1.data.social.FacebookAPI;
import vn.com.engma.esmarketDemo1.data.social.GooglePlusAPI;
import vn.com.engma.esmarketDemo1.ui.base.Presenter;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseCallback;
import vn.com.engma.esmarketDemo1.ui.component.fragment.news_fragment.NewsFragmentContract;
import vn.com.engma.esmarketDemo1.use_case.UserUseCase;
import vn.com.engma.esmarketDemo1.utils.Constants;
import vn.com.engma.esmarketDemo1.utils.L;
import vn.com.engma.esmarketDemo1.utils.ParamsConstants;


/**
 * Created by TuanNguyen on 15/12/2017
 */

public class HomeFragmentPresenter extends Presenter<HomeFragmentContract.View> implements HomeFragmentContract.Presenter {

    private UserUseCase useCase;
    private GooglePlusAPI googlePlusAPI;
    private FacebookAPI facebookAPI;

    @Inject
    public HomeFragmentPresenter(UserUseCase demoUseCase) {
        this.useCase = demoUseCase;

    }



    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
        super.initialize(extras);
        Map<String,String> map = new HashMap<>();
        map.put(ParamsConstants.LIMIT_RECORD, Constants.LIMIT_RECORD_DEFAULT);
        map.put(ParamsConstants.OFFSET,Constants.OFFSET_DEFAULT);
        map.put(ParamsConstants.LIMIT,Constants.LIMIT_DEFAULT);
        map.put(ParamsConstants.IS_FEATURE,Constants.IS_FEATURE_DEFAULT);
        useCase.requestCategory(map, new BaseCallback() {
            @Override
            public void onSuccess(ERespond eRespond) {
                //List<CagetoryItem> cagetoryItems = ((ModelCagetory)eRespond).getProductItemList();
                //getView().displayProduct(cagetoryItems);
                handleResultCategory(((ModelCagetory) eRespond));

            }

            @Override
            public void onFail(Throwable error) {
                getView().showErrorMessage(R.string.login_travel_s02_err_try_again);
                L.e("Hoan",error.toString());
            }
        });

    }
    @Override
    public void getExtra(Activity activity) {

    }

    @Override
    public void getCategory(HashMap<String, String> params) {
        useCase.requestCategory(params,new BaseCallback(){
            @Override
            public void onSuccess(ERespond respondUser) {
                handleResultCategory(((ModelCagetory) respondUser));
            }

            @Override
            public void onFail(Throwable error) {
                getView().showErrorMessage(R.string.login_travel_s02_err_try_again);
                L.e("Hoan",error.toString());
            }
        });
    }

    private void handleResultCategory (ModelCagetory categoryModelRespond)
    {
        if(categoryModelRespond.getStatus()==ERespond.OK){
            getView().displayProductPMHome(categoryModelRespond);
        }
    }
}
