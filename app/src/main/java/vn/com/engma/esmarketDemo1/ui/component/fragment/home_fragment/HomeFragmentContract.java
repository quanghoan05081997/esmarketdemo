package vn.com.engma.esmarketDemo1.ui.component.fragment.home_fragment;

import java.util.HashMap;

import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;


/**
 * Created by tuannguyen on 13/12/17.
 */

public interface HomeFragmentContract {
    interface View extends BaseView {

       void displayProductPMHome(ModelCagetory modelCagetory);
    }

    interface Presenter {

        void getCategory (HashMap<String, String> params);
    }
}
