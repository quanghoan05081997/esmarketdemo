package vn.com.engma.esmarketDemo1;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.VisibleForTesting;
import android.support.multidex.MultiDexApplication;

import vn.com.engma.esmarketDemo1.di.DaggerMainComponent;
import vn.com.engma.esmarketDemo1.di.MainComponent;


/**
 * Created by Dev.TuanNguyen on 12/10/2017.
 */

public class App extends MultiDexApplication {
    private MainComponent mainComponent;
    private static Context context;
    private static App singleton;

    @Override
    public void onCreate() {
        super.onCreate();
        mainComponent = DaggerMainComponent.create();
        context = getApplicationContext();
    }

    public static App getInstance() {
        return singleton;
    }


    public MainComponent getMainComponent() {
        return mainComponent;
    }

    public static Context getContext() {
        return context;
    }

    @VisibleForTesting
    public void setComponent(MainComponent mainComponent) {
        this.mainComponent = mainComponent;
    }

    public SharedPreferences getSettingPreference() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }
}
