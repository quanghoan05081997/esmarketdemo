package vn.com.engma.esmarketDemo1.ui.component.splash_s01;


import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

import javax.inject.Inject;

import butterknife.BindView;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.ui.base.BaseActivity;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;

import static vn.com.engma.esmarketDemo1.utils.Constants.SPLASH_DELAY;

/**
 * Created by tuannguyen on 13/12/17.
 */

public class SplashActivity extends BaseActivity implements SplashContract.View {

    @Inject
    SplashPresenter splashPresenter;

    @BindView(R.id.imgLogo)
    ImageView imgLogo;

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(SplashActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = splashPresenter;
        presenter.setView(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.splash_s01_activity;
    }


    @Override
    public void navigateToMainScreen() {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, vn.com.engma.esmarketDemo1.ui.component.login_esmarket_s01.LoginActivity.class);
            startActivity(intent);
            finish();
        }, SPLASH_DELAY);
    }

    @Override
    public void navigateToLogin() {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, vn.com.engma.esmarketDemo1.ui.component.login_esmarket_s01.LoginActivity.class);
            startActivity(intent);
            finish();
        }, SPLASH_DELAY);
    }

    @Override
    public void runAnimation() {
        AlphaAnimation animation = new AlphaAnimation(0.1f, 1.0f);
        animation.setDuration(600);
        animation.setStartOffset(300);
        animation.setFillAfter(true);
        imgLogo.startAnimation(animation);
    }

    @Override
    public void navigateToHomeScreen() {

    }

    @Override
    public void showDialog() {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void showErrToast(int errMessage) {
        CommonUtils.showAppToast(SplashActivity.this, errMessage);
    }

    @Override
    public void showErrorMessage(int errMessage) {

    }

    @Override
    public void showPopupMenu() {

    }

    @Override
    public void showToast(int errMessage) {

    }

    @Override
    public void showPopupMenu(View view) {

    }
}
