package vn.com.engma.esmarketDemo1.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;
import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomeActivity;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;

/**
 * Created by Dev.TuanNguyen on 12/10/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView{

        protected Presenter presenter;
        private Unbinder unbinder;

        ProgressDialog progressDialog;

        protected BaseActivity activity;
        protected App app;


        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            {
                View layout = null;
                Bundle bundle = getArguments();
                activity = (HomeActivity) getActivity();
                app = (App) activity.getApplicationContext();
                layout = inflater.inflate(getLayoutId(), container, false);
                unbinder = ButterKnife.bind(this, layout);
                initializeDagger();
                initializePresenter();
                if (presenter != null) {
                    presenter.setActivity(activity);
                    presenter.setFragment(this);
                    presenter.initialize(bundle);
                }

                return layout;
            }
        }


        protected abstract void initializeDagger();

        protected abstract void initializePresenter();

        public abstract int getLayoutId();

        @Override
        public void onDestroy() {
            super.onDestroy();
        }

        @Override
        public void showToast(int errMessage) {
            CommonUtils.showAppToast(getActivity(), errMessage);
        }



        @Override
        public void showPopupMenu(View view) {

        }


        @Override
        public void navigateToHomeScreen() {

        }

        @Override
        public void showDialog() {
            dismissDialog();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.mess_loading));
            progressDialog.show();
        }

        @Override
        public void dismissDialog() {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    @Override
    public void showErrToast(int errMessage) {

    }

    @Override
    public void showErrorMessage(int errMessage) {

    }
}
