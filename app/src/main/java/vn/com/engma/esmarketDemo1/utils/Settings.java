package vn.com.engma.esmarketDemo1.utils;

/**
 * Created by nhanmai on 1/15/18.
 */

public class Settings {

    //        public final static String DOMAIN_SERVER = "http://192.168.0.88";
    public final static String DOMAIN_SERVER = "http://demo1.engma.com.vn";
//    public final static String DOMAIN_SERVER = "http://demo2.hoidapsuckhoeonline.com";
//    public final static String DOMAIN_SERVER = "http://praywish.com";


    //    public final static String PORT_SERVER = "8080";
    public final static String PORT_SERVER = "80";

    //		public final static String URL_SERVER = "http://hoidapsuckhoeonline.com/";
//	public final static String URL_SERVER = "http://192.168.33.59:8080/";
//	public final static String URL_SERVER = "http://192.168.33.14:8080/";
    public final static String URL_SERVER = DOMAIN_SERVER + "/";

    public final static Boolean PRINT_TO_LOGCAT = true;
    public final static Boolean SHOW_ERROR_ALERT = false;
    public final static String TAG_DEBUG = "TAG_DEBUG";
    public final static int SPEED_RUNNING_CARD = 150;
    public final static float SPEED_LINNEAR_MANAGER = 4000f; //Change this value (default=25f)

    public static boolean GET_KEYHASH_RELEASE = false;
}
