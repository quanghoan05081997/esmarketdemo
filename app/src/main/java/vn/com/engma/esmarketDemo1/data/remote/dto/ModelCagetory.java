package vn.com.engma.esmarketDemo1.data.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import vn.com.engma.esmarketDemo1.data.remote.dto.base.EObject;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;

/**
 * Created by nhanmai on 1/15/18.
 */

public class ModelCagetory extends ERespond {
    public static class CategoryModelRespond extends ERespond {
        ModelCagetory data;

        public ModelCagetory getData() {
            return data;
        }
    }



    @SerializedName("data")

    private List<CagetoryItem> productItemList = new ArrayList<>();

    public List<CagetoryItem> getProductItemList() {
        return productItemList;
    }

    public void setProductItemList(List<CagetoryItem> productItemList) {
        this.productItemList = productItemList;
    }
}
