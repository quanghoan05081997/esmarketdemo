package vn.com.engma.esmarketDemo1.data.remote.dto.base;

/**
 * Created by nhanmai on 7/13/17.
 */

public class HttpMessage {
    public static final String USER_ALREADY_EXISTS = "USER_ALREADY_EXISTS";
    public static final String EMAIL_AVAILABLE = "EMAIL_IS_AVAILABLE";
    public static final String WRONG_CAPTCHA_OLD_DATE = "WRONG_CAPTCHA_OLD_DATE";
    public static final String WRONG_CAPTCHA_OLD_TIME = "WRONG_CAPTCHA_OLD_TIME";
    public static final String WRONG_CAPTCHA = "WRONG_CAPTCHA";
    public static final String PHONE_AVAILABLE = "PHONE_AVAILABLE";
    public static final String LOGIN_FAILED = "LOGIN_FAILED";
    public static final String NOT_FOUND = "NOT_FOUND";
    public static final String OK = "OK";
    public static final String BANNED = "BANNED";
    public static final String NON_ACTIVATE = "NON_ACTIVATE";
    public static final String UNVALIDATED = "UNVALIDATED";
    public static final String MISMATCH_PARAMS = "MISMATCH_PARAMS";
    public static final String ALREADY_REQUEST = "ALREADY_REQUEST";
    public static final String NOT_LOGIN = "NOT_LOGIN";
    public static final String ERROR_TYPE_FORMAT = "ERROR_TYPE_FORMAT";
    public static final String SESSION_FILE_IS_EMPTY = "SESSION_FILE_IS_EMPTY";
    public static final String EXCEED_NUMER_CLICK_PRAYWISH_TODAY = "EXCEED_NUMER_CLICK_PRAYWISH_TODAY";
    public static final String FORBIDDEN = "FORBIDDEN";
    public static final String BAD_REQUEST = "BAD_REQUEST";
    public static final String ALREADY_GROUP_MEMBER = "ALREADY_GROUP_MEMBER";

}
