package vn.com.engma.esmarketDemo1.utils;

/**
 * Created by nhanmai on 7/12/17.
 */

public class ParamsConstants {

    public static final String OBJECT_ID = "_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String BIRTHDAY = "birthday";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String LAST_ID = "last_id";
    public static final String USER_ID = "user_id";
    public static final String USER_ACCOUNT_ID = "user_account_id";
    public static final String USER_ACCOUNT_IDS = "user_account_ids";
    public static final String AVATAR = "avatar";
    public static final String GENDER = "gender";
    public static final String FB_ID = "fb_id";
    public static final String FB_IMG_SRC = "fb_img_src";
    public static final String GG_ID = "gg_id";
    public static final String GG_IMG_SRC = "gg_img_src";
    public static final String BG_SRC = "bg_src";
    public static final String DROP_BG_SRC = "drop_bg_src";
    public static final String IMG_SRC = "img_src";
    public static final String FILE = "file";
    public static final String THUMBNAIL_FILE = "thumbnail_file";
    public static final String DROP_IMG_SRC = "drop_img_src";
    public static final String MARITAL_STATUS_ID = "marital_status_id";
    public static final String HOME_TOWN = "home_town";
    public static final String MOTTO_STATUS = "motto_status";
    public static final String KNOWLEDGE_LANGUAGES = "knowledge_languages";
    public static final String LOCATION = "location";
    public static final String LOGIN_TYPE = "login_type";
    public static final String NOTIFICATION_IDS = "notification_ids";

    public static final String IS_EMAIL_PUBLIC = "is_email_public";
    public static final String IS_FIRST_NAME_PUBLIC = "is_first_name_public";
    public static final String IS_LAST_NAME_PUBLIC = "is_last_name_public";
    public static final String IS_ADDRESS_PUBLIC = "is_address_public";
    public static final String IS_PHONE_PUBLIC = "is_phone_public";
    public static final String IS_GENDER_PUBLIC = "is_gender_public";
    public static final String IS_BIRTHDAY_PUBLIC = "is_birthday_public";
    public static final String IS_HOME_TOWN_PUBLIC = "is_home_town_public";
    public static final String IS_COUNTRY_PUBLIC = "is_country_public";
    public static final String IS_PROVINCE_PUBLIC = "is_province_public";
    public static final String IS_CAREER_PUBLIC = "is_career_public";
    public static final String IS_REGENCY_PUBLIC = "is_regency_public";
    public static final String IS_DEGREE_PUBLIC = "is_degree_public";
    public static final String IS_MARITAL_STATUS_PUBLIC = "is_marital_status_public";
    public static final String IS_RELIGION_PUBLIC = "is_religion_public";
    public static final String IS_KNOWLEDGE_LANGUAGES_PUBLIC = "is_knowledge_languages_public";
    public static final String IS_MOTTO_STATUS_PUBLIC = "is_motto_status_public";
    public static final String IS_OWNER_PUBLIC = "is_owner_public";
    public static final String IS_GROUP_PUBLIC = "is_public";

    //other
    public static final String PAGE = "page";
    public static final String LATEST_UPDATE_TIME = "time";
    public static final String TYPE = "type";
    public static final String LINK = "link";
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    public static final String CODE = "code";

    public static final String KEY_WORD = "keyword";
    public static final String OFFSET_RESP = "offset_resp";
    public static final String DATA_LIST = "data_list";
    public static final String CAPTCHA = "captcha";
    //check GENDER
    public static final String GENDER_TYPE = "gender_type";
    public static final String PARAM_RELIGION = "RELIGION";
    public static final String PARAM_OWNER = "OWNER";
    public static final String PARAM_RELIGION_ID = "religion_id";
    public static final String PARAM_OWNER_ID = "owner";
    public static final String PARAM_PARENT_ID = "parent_id";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_PROVINCE_ID = "province_id";
    public static final String PARAM_GENDER_ID = "gender_id";
    public static final String PARAM_DEGREE = "degree";
    public static final String PARAM_CAREER = "career";
    public static final String PARAM_REGENCY = "regency";
    public static final String PARAM_ADDRESS = "address";

    public static final String PARAM_PHONE_ID = "phone_id";
    public static final String PARAM_PHONE_VERSION = "phone_version";
    public static final String PARAM_PHONE_NAME = "phone_name";

    public static final String PARAM_APPLICATION_ID = "app_id";
    public static final String PARAM_APP_VERSION = "app_version";


    //Post pray wish
    public static final String PARAM_POST_TYPE_ID = "post_type_id";
    public static final String PARAM_DATA_FILES = "data_files";
    public static final String PARAM_SELECTED_INDEX = "PARAM_SELECTED_INDEX";

    public static final String PARAM_ID = "id";
    public static final String PARAM_POST_TYPE_NAME = "name";
    public static final String PARAM_POST_IS_PUBLIC = "is_public";
    public static final String PARAM_POST_IS_CONTENTED = "is_contented";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_CREATED_AT = "created_at";
    public static final String PARAM_CONTENT = "content";
    public static final String PARAM_POST_CONTENT = "post_content";
    public static final String PARAM_FILES = "files";
    public static final String PARAM_FILE_SRC = "file_src";
    public static final String PARAM_FILE_SIZE = "file_size";
    public static final String PARAM_AUTHORIZATION = "Authorization";
    public static final String PARAM_LANGUAGE = "Language";
    public static final String PARAM_CONTENT_TYPE = "Content-Type";
    public static final String DENY_FRIEND = "deny";
    public static final String CANCEL_FRIEND = "cancel";
    public static final String ACCEPT_FRIEND = "accept";
    public static final String POST_ID = "post_id";
    public static final String COMMENT_ID = "comment_id";
    public static final String PARAM_REPORT_ID = "report_id";


    //Get list posted

    public static final String PARAM_IMG_SRC = "img_src";
    public static final String PARAM_DROP_IMG_SRC = "drop_img_src";
    //message
    public static final String PARAM_MESSAGE = "message";
    public static final String CONVERSATION_ID = "conversation_id";
    public static final String PARAM_IMAGE = "image";

    public static final String PARAM_SCOPE_TYPE = "scope_type";
    public static final String PARAM_TAGGED_USER_ACCOUNT_IDS = "tagged_user_account_ids";
    public static final String PARAM_STICKER_ID = "sticker_id";

    //push notification
    public static final String PUSH_FIREBASE_TOKEN = "push_firebase_token";
    public static final String PARAM_ACTION_KEY = "action_key";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_TEXT = "text";
    public static final String PARAM_OBJ_ID = "obj_id";

    //Company, family, community
    public static final String PARAM_GROUP_TYPE_ID = "group_type_id";
    public static final String PARAM_GROUP_ID = "group_id";
    public static final String PARAM_MEMBER_IDS = "member_ids";
    public static final String PARAM_MEMBER_ALIAS_NAMES = "member_alias_names";

    //TOP
    public static final String PARAM_FROM_DATE = "from_date";
    public static final String PARAM_TO_DATE = "to_date";
    public static final String TYPE_NAME = "type_name";

    public static final String GEOLOCATION_IDS = "geolocation_ids";

    //KingDom word
    public static final String PARAM_WWORD_CATEGORY_ID = "wword_category_id";
    public static final String PARAM_WWORD_POST_ID = "wword_post_id";

    //Time setting alarm praywish
    public static final String PARAM_TIMES = "times";

    //PMS
    public static final String PARAM_PMS_SUBJECT_ID = "pms_subject_id";
    public static final String PARAM_PMS_ENVELOPE_ID = "pms_envelope_id";

    //Rating
    public static final String PARAM_SCORE = "score";


    public static final String PARAM_EXT = "ext";


    //Change Password
    public static final String PARAM_CURRENT_PASSWORD = "current_password";
    public static final String PARAM_NEW_PASSWORD = "new_password";


    public static final String PARAM_ACTION_HIDE_POST_ID = "action_hide_post_id";


    public static final String PARAM_ID_LANGUAGE = "language_id";
    public static final String DEPARTED_DATE = "anniversary_of_death";
    public static final String PROFILE = "profile";
    public static final String DECEASED_ID = "deceased_id";      //id of person in Noi Tho Cung


    public static final String IS_CLICK_MORE = "IS_CLICK_MORE";
    public static final String PARAM_DEVICE_ID = "device_id";
    public static final String LIMIT_RECORD = "limit_record";
    public static final String IS_FEATURE ="is_feature";
}
