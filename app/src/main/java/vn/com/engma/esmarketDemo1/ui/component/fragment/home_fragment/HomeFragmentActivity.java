package vn.com.engma.esmarketDemo1.ui.component.fragment.home_fragment;

import android.support.annotation.Nullable;
import android.widget.ListView;
import android.widget.ViewFlipper;

import javax.inject.Inject;

import butterknife.BindView;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.ui.base.BaseFragment;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by pc-computer on 5/17/2018.
 */

public class HomeFragmentActivity extends BaseFragment implements HomeFragmentContract.View {

    @Inject
    HomeFragmentPresenter presenter;

    @Nullable
    @BindView(R.id.lvCategory)
    ListView listViewCategory;

    @Nullable
    @BindView(R.id.viewFlipper)
    ViewFlipper viewFlipper;

    @Override
    public void showPopupMenu() {

    }

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(HomeFragmentActivity.this);

    }

    @Override
    protected void initializePresenter() {

        super.presenter=presenter;
        presenter.setView(this);
        CommonUtils.hideKeyboard(activity);

        viewFlipper.setFlipInterval(3000);
        viewFlipper.setAutoStart(true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.home_fragment_s06_layout;
    }

    @Override
    public void displayProductPMHome(ModelCagetory modelCagetory) {
        AdapterHome adapterHome = new AdapterHome(getContext(),modelCagetory);
        listViewCategory.setAdapter(adapterHome);
    }
}
