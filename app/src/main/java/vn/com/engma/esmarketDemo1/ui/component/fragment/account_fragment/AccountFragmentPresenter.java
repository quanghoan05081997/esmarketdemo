package vn.com.engma.esmarketDemo1.ui.component.fragment.account_fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;
import vn.com.engma.esmarketDemo1.data.social.FacebookAPI;
import vn.com.engma.esmarketDemo1.data.social.GooglePlusAPI;
import vn.com.engma.esmarketDemo1.ui.base.Presenter;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseCallback;
import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomeContract;
import vn.com.engma.esmarketDemo1.use_case.UserUseCase;
import vn.com.engma.esmarketDemo1.utils.Constants;
import vn.com.engma.esmarketDemo1.utils.L;
import vn.com.engma.esmarketDemo1.utils.ParamsConstants;


/**
 * Created by TuanNguyen on 15/12/2017
 */

public class AccountFragmentPresenter extends Presenter<AccountFragmentContract.View> implements AccountFragmentContract.Presenter {

    private UserUseCase useCase;
    private GooglePlusAPI googlePlusAPI;
    private FacebookAPI facebookAPI;

    @Inject
    public AccountFragmentPresenter(UserUseCase demoUseCase) {
        this.useCase = demoUseCase;

    }

//    @Inject
//    public LoginPresenter() {
//
//    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
    }
    @Override
    public void getExtra(Activity activity) {

    }




}
