package vn.com.engma.esmarketDemo1.use_case;


import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import vn.com.engma.esmarketDemo1.data.DataRepository;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.User;
import vn.com.engma.esmarketDemo1.data.social.LoginType;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseCallback;

public class UserUseCase implements UseCase {
    private DataRepository dataRepository;
    private CompositeDisposable compositeDisposable;
    private Disposable newsDisposable;
    //    Single<NewsModel> newsModelSingle;
    Single<User.UserRespond> userRespondSingle;

    private DisposableSingleObserver<User.UserRespond> disposableSingleObserver;

    @Inject
    public UserUseCase(DataRepository dataRepository, CompositeDisposable compositeDisposable) {
        this.dataRepository = dataRepository;
        this.compositeDisposable = compositeDisposable;
    }

    public void login_social(LoginType loginType, String socialId, String email, String birthday, String first_name, String last_name, String gender, String avatar, BaseCallback callback)
    {
        disposableSingleObserver = new DisposableSingleObserver<User.UserRespond>() {
            @Override
            public void onSuccess(User.UserRespond userRespond) {
                callback.onSuccess(userRespond);
            }

            @Override
            public void onError(Throwable throwable) {
                callback.onFail(throwable);
            }
        };
    }

    @Override
    public void login(String username, String password, BaseCallback callback) {
        disposableSingleObserver = new DisposableSingleObserver<User.UserRespond>() {
            @Override
            public void onSuccess(User.UserRespond userRespond) {
                callback.onSuccess(userRespond);
            }

            @Override
            public void onError(Throwable e) {
                callback.onFail(e);
            }
        };

        if (!compositeDisposable.isDisposed()) {
            userRespondSingle = dataRepository.requestLogin(username, password);

            newsDisposable = userRespondSingle.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeWith(disposableSingleObserver);
            compositeDisposable.add(newsDisposable);
        }
    }

    @Override
    public void requestCategory(Map<String, String> map, BaseCallback callback) {
        DisposableSingleObserver<ModelCagetory> disposableSingleObserver = new DisposableSingleObserver<ModelCagetory>() {
            @Override
            public void onSuccess(ModelCagetory modelCagetoryRespond) {
                callback.onSuccess(modelCagetoryRespond);
            }

            @Override
            public void onError(Throwable e) {
                callback.onFail(e);
            }
        };

        if (!compositeDisposable.isDisposed()) {
            Single<ModelCagetory> userRespondSingle = dataRepository.requestGetCategory(map);

            newsDisposable = userRespondSingle.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeWith(disposableSingleObserver);
            compositeDisposable.add(newsDisposable);
        }
    }

    public void unSubscribe() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.remove(newsDisposable);
        }
    }
}
