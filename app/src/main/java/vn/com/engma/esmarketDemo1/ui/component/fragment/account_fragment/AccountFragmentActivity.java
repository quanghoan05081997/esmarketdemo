package vn.com.engma.esmarketDemo1.ui.component.fragment.account_fragment;


import android.content.Intent;
import android.view.View;

import javax.inject.Inject;

import butterknife.OnClick;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.ui.base.BaseActivity;
import vn.com.engma.esmarketDemo1.ui.base.BaseFragment;
import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomeContract;
import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomePresenter;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by tuannguyen on 13/12/17.
 */

public class AccountFragmentActivity extends BaseFragment implements AccountFragmentContract.View {



    @Inject
    AccountFragmentPresenter presenter;



    @Override
    public int getLayoutId() {

        return R.layout.account_fragment_s06_layout;
    }

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(AccountFragmentActivity.this);
        
        
    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        CommonUtils.hideKeyboard(activity);

    }

    @OnClick({})
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }


    @Override
    public void showErrorMessage(int errMessage) {

    }

    @Override
    public void showPopupMenu() {

    }


}
