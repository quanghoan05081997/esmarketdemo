package vn.com.engma.esmarketDemo1.ui.component.splash_s01;


import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface SplashContract {
    interface View extends BaseView {
        void navigateToMainScreen();
        void navigateToLogin();
        void runAnimation();
    }

    interface Presenter {

    }
}
