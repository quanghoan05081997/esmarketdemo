package vn.com.engma.esmarketDemo1.ui.component.fragment.home_fragment;

import java.util.ArrayList;

/**
 * Created by pc-computer on 5/18/2018.
 */

public class Product {
    String tilte;
    ArrayList<Product_con> arrayList;

    public Product(String tilte, ArrayList<Product_con> arrayList) {
        this.tilte = tilte;
        this.arrayList = arrayList;
    }

    public String getTilte() {
        return tilte;
    }

    public void setTilte(String tilte) {
        this.tilte = tilte;
    }

    public ArrayList<Product_con> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Product_con> arrayList) {
        this.arrayList = arrayList;
    }

    public static class Product_con{
        private int imgProduct;
        private String tvPrice;
        private String tvSalePrice;

        public Product_con(int imgProduct, String tvPrice, String tvSalePrice) {
            this.imgProduct = imgProduct;
            this.tvPrice = tvPrice;
            this.tvSalePrice = tvSalePrice;
        }

        public int getImgProduct() {
            return imgProduct;
        }

        public void setImgProduct(int imgProduct) {
            this.imgProduct = imgProduct;
        }

        public String getTvPrice() {
            return tvPrice;
        }

        public void setTvPrice(String tvPrice) {
            this.tvPrice = tvPrice;
        }

        public String getTvSalePrice() {
            return tvSalePrice;
        }

        public void setTvSalePrice(String tvSalePrice) {
            this.tvSalePrice = tvSalePrice;
        }
    }
}
