package vn.com.engma.esmarketDemo1.data.social;

/**
 * Created by nhanmai on 1/16/18.
 */

public interface LoginSocialResult {
    void loginResult(LoginType loginType, String socialId, String email, String birthday, String first_name, String last_name, String gender, String avatar);

    void errorResult(LoginType loginType, Throwable error);
}
