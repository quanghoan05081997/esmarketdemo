package vn.com.engma.esmarketDemo1.data.social;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import vn.com.engma.esmarketDemo1.utils.L;

/**
 * Created by nhanmai on 1/16/18.
 */

public class FacebookAPI {

    private final static int LIMIT_RETRY = 3;

    private AppCompatActivity activity;
    private LoginSocialResult loginSocialResult;

    private int retryCount = 0;

    //facebook var
    private CallbackManager callbackManager;
    final List<String> permissionNeeds = Arrays.asList("user_photos", "email",
            "user_birthday", "public_profile");

    public FacebookAPI(@NonNull AppCompatActivity activity, @NonNull LoginSocialResult loginSocialResult) {
        this.activity = activity;
        this.loginSocialResult = loginSocialResult;

        FacebookSdk.sdkInitialize(activity.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
        AccessToken.getCurrentAccessToken();

        LoginManager.getInstance().registerCallback(callbackManager, facebookCallback);


    }

    public void signIn() {
        signIn(true);
    }

    public void signIn(boolean isRetry) {
        if (isRetry) {
            retryCount = 0;
        } else {
            retryCount = 3;
        }
        processLogin();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void processLogin() {
        if (AccessToken.getCurrentAccessToken() != null) {
            loginFbAccount(AccessToken.getCurrentAccessToken());
        } else {
            LoginManager.getInstance().logInWithReadPermissions(activity, permissionNeeds);
        }
    }

    private void retryLogin() {
        if (retryCount < LIMIT_RETRY) {
            retryCount++;
            processLogin();
        } else {
            if (loginSocialResult != null) {
                loginSocialResult.errorResult(LoginType.FB, new Exception("Login Failed"));
            }
        }
    }


    private void loginFbAccount(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {

                        Profile currentProfile = Profile.getCurrentProfile();
                        if (currentProfile != null && !TextUtils.isEmpty(currentProfile.getId())) {
                            try {
                                Uri urlImgSrc = currentProfile.getProfilePictureUri(300, 300);
                                String email = object.has("email") ? object.getString("email") : "";
                                String birthday = object.has("birthday") ? object.getString("birthday") : "";
                                String gender = object.has("gender") ? object.getString("gender") : "";
                                String first_name = currentProfile.getFirstName();
                                String last_name = currentProfile.getLastName();

                                if (loginSocialResult != null) {
                                    loginSocialResult.loginResult(LoginType.FB, currentProfile.getId(), email, birthday, first_name, last_name, gender, urlImgSrc != null ? urlImgSrc.toString() : "");
                                }

                            } catch (Exception e) {
                                if (loginSocialResult != null) {
                                    loginSocialResult.errorResult(LoginType.FB, e);
                                }
                            }
                        } else {
                            retryLogin();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, link, email, birthday, gender");
        request.setParameters(parameters);
        request.executeAsync();
    }


    FacebookCallback facebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            loginFbAccount(loginResult.getAccessToken());
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {
            L.e("FacebookCallback", error.getMessage());
        }
    };
}
