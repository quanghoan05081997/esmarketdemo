
package vn.com.engma.esmarketDemo1.data.remote.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class EvaluateItem implements Parcelable {

    @SerializedName("avatar_src")
    private String avatar_src;

    @SerializedName("user_name")
    private String user_name;

    @SerializedName("date")
    private String date;

    @SerializedName("comment")
    private String comment;

    @SerializedName("rate")
    private float rate;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.avatar_src);
        dest.writeString(this.user_name);
        dest.writeString(this.date);
        dest.writeString(this.comment);
        dest.writeFloat(this.rate);
    }

    public EvaluateItem() {
    }

    protected EvaluateItem(Parcel in) {
        this.avatar_src = in.readString();
        this.user_name = in.readString();
        this.date = in.readString();
        this.comment = in.readString();
        this.rate = in.readFloat();
    }

    public static final Creator<EvaluateItem> CREATOR = new Creator<EvaluateItem>() {
        @Override
        public EvaluateItem createFromParcel(Parcel source) {
            return new EvaluateItem(source);
        }

        @Override
        public EvaluateItem[] newArray(int size) {
            return new EvaluateItem[size];
        }
    };

    public String getAvatar_src() {
        return avatar_src;
    }

    public void setAvatar_src(String avatar_src) {
        this.avatar_src = avatar_src;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public static Creator<EvaluateItem> getCREATOR() {
        return CREATOR;
    }
}
