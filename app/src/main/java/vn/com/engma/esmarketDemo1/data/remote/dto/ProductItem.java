package vn.com.engma.esmarketDemo1.data.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import vn.com.engma.esmarketDemo1.data.remote.dto.base.EObject;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;
import vn.com.engma.esmarketDemo1.utils.Settings;

/**
 * Created by nhanmai on 1/15/18.
 */

public class ProductItem extends EObject {
    public static class UserRespond extends ERespond {
        ProductItem data;
        public ProductItem getData() {
            return data;
        }
    }

    @Expose
    @SerializedName("code")
    String code;

    @Expose
    @SerializedName("img_src")
    String img_src;

    @Expose
    @SerializedName("title")
    String title;

    @Expose
    @SerializedName("price")
    String price;


    @Expose
    @SerializedName("sale_price")
    String sale_price;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImg_src() {
//        return img_src;

        return Settings.URL_SERVER + img_src;
    }

    public void setImg_src(String img_src) {
        this.img_src = img_src;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }
}
