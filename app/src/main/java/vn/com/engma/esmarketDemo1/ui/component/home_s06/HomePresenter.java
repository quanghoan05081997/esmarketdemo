package vn.com.engma.esmarketDemo1.ui.component.home_s06;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.CagetoryItem;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;
import vn.com.engma.esmarketDemo1.data.social.FacebookAPI;
import vn.com.engma.esmarketDemo1.data.social.GooglePlusAPI;
import vn.com.engma.esmarketDemo1.ui.base.Presenter;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseCallback;
import vn.com.engma.esmarketDemo1.use_case.UserUseCase;
import vn.com.engma.esmarketDemo1.utils.Constants;
import vn.com.engma.esmarketDemo1.utils.L;
import vn.com.engma.esmarketDemo1.utils.ParamsConstants;


/**
 * Created by TuanNguyen on 15/12/2017
 */

public class HomePresenter extends Presenter<HomeContract.View> implements HomeContract.Presenter {

    private UserUseCase useCase;
    private GooglePlusAPI googlePlusAPI;
    private FacebookAPI facebookAPI;

    @Inject
    public HomePresenter(UserUseCase demoUseCase) {
        this.useCase = demoUseCase;

    }

//    @Inject
//    public LoginPresenter() {
//
//    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
//        Map<String,String> map = new HashMap<>();
//        map.put(ParamsConstants.LIMIT_RECORD, Constants.LIMIT_RECORD_DEFAULT);
//        map.put(ParamsConstants.OFFSET,Constants.OFFSET_DEFAULT);
//        map.put(ParamsConstants.LIMIT,Constants.LIMIT_DEFAULT);
//        map.put(ParamsConstants.IS_FEATURE,Constants.IS_FEATURE_DEFAULT);
//        useCase.requestCategory(map, new BaseCallback() {
//            @Override
//            public void onSuccess(ERespond eRespond) {
//                //List<CagetoryItem> cagetoryItems = ((ModelCagetory)eRespond).getProductItemList();
//                //getView().displayProduct(cagetoryItems);
//                handleResultCategory(((ModelCagetory) eRespond));
//
//            }
//
//            @Override
//            public void onFail(Throwable error) {
//                getView().showErrorMessage(R.string.login_travel_s02_err_try_again);
//                L.e("Hoan",error.toString());
//            }
//        });
    }
    @Override
    public void getExtra(Activity activity) {

    }

//    @Override
//    public void getCategory(HashMap<String, String> params) {
//        useCase.requestCategory(params,new BaseCallback(){
//            @Override
//            public void onSuccess(ERespond respondUser) {
//                handleResultCategory(((ModelCagetory) respondUser));
//            }
//
//            @Override
//            public void onFail(Throwable error) {
//                getView().showErrorMessage(R.string.login_travel_s02_err_try_again);
//                L.e("Hoan",error.toString());
//            }
//        });
//    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookAPI.onActivityResult(requestCode, resultCode, data);
    }

//    private void handleResultCategory(ModelCagetory categoryModelRespond) {
//        if (categoryModelRespond.getStatus() == ERespond.OK) {
//            //@// TODO: 1/18/18 save login user
//            getView().displayProduct(categoryModelRespond);
//        }
//    }
}
