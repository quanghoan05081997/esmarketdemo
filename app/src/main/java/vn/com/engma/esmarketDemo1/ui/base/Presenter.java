package vn.com.engma.esmarketDemo1.ui.base;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;


/**
 * Created by tuannguyen on 13/12/17.
 */


public abstract class Presenter<T extends BaseView> {

    private WeakReference<T> view;
    protected BaseActivity activity;
    protected BaseFragment fragment;
    private SharedPreferences sharedPreferences;

    protected AtomicBoolean isViewAlive = new AtomicBoolean();

    public T getView() {
        return view.get();
    }

    public void setView(T view) {
        this.view = new WeakReference<>(view);
    }

    public void setActivity(BaseActivity activity) {
        this.activity = activity;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
//        sharedPreferences = App.getInstance().getSettingPreference();
    }



    public void initialize(Bundle extras) {
    }

    public abstract void getExtra(Activity activity);

    public void start() {
        isViewAlive.set(true);
    }

    public void finalizeView() {
        isViewAlive.set(false);
    }


}
