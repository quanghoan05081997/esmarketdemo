package vn.com.engma.esmarketDemo1.di;


import javax.inject.Singleton;

import dagger.Component;
import vn.com.engma.esmarketDemo1.ui.component.fragment.account_fragment.AccountFragmentActivity;
import vn.com.engma.esmarketDemo1.ui.component.fragment.home_fragment.HomeFragmentActivity;
import vn.com.engma.esmarketDemo1.ui.component.fragment.news_fragment.NewsFragmentActivity;
import vn.com.engma.esmarketDemo1.ui.component.fragment.shoppingcart_fragment.ShoppingcartFragmentActivity;
import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomeActivity;
import vn.com.engma.esmarketDemo1.ui.component.splash_s01.SplashActivity;

/**
 * Created by AhmedEltaher on 5/12/2016
 */
@Singleton
@Component(modules = MainModule.class)
public interface MainComponent {
    void inject(SplashActivity activity);

    //
    void inject(vn.com.engma.esmarketDemo1.ui.component.login_travel_s01.LoginActivity activity);

    void inject(vn.com.engma.esmarketDemo1.ui.component.login_travel_s02.LoginActivity activity);

    void inject(vn.com.engma.esmarketDemo1.ui.component.login_travel_s05.LoginActivity activity);

    void inject(vn.com.engma.esmarketDemo1.ui.component.login_esmarket_s01.LoginActivity activity);

    void inject(HomeActivity homeActivity);

    void inject(NewsFragmentActivity newsFragmentActivity);

    void inject(ShoppingcartFragmentActivity shoppingcartFragmentActivity);

    void inject(AccountFragmentActivity accountFragmentActivity);


    void inject(HomeFragmentActivity homeFragmentActivity);
//
//    void inject(ForgotPasswordActivity activity);
//
//    void inject(RegisterActivity activity);
//
//    void inject(HomeActivity activity);
//
//    void inject(ServiceListActivity activity);
//
//    void inject(ChangePasswordActivity activity);
//
//    void inject(ServiceDetailActivity activity);
//
//    void inject(ConfirmOrderActivity activity);
//
//    void inject(ServiceManagerActivity activity);
//
//    void inject(UpdateProfileActivity activity);
//
//    void inject(QuestionManagerActivity activity);
//
//    void inject(QuestionDetailActivity activity);
//
//    void inject(NotificationManagerActivity activity);
}
