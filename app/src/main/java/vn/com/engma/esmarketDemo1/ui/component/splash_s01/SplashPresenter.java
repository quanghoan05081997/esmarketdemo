package vn.com.engma.esmarketDemo1.ui.component.splash_s01;

import android.app.Activity;
import android.os.Bundle;

import javax.inject.Inject;

import vn.com.engma.esmarketDemo1.ui.base.Presenter;


/**
 * Created by TuanNguyen on 15/12/2017
 */

public class SplashPresenter extends Presenter<SplashContract.View> implements SplashContract.Presenter {

    @Inject
    public SplashPresenter() {
    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
        getView().navigateToMainScreen();
        getView().runAnimation();
    }

    @Override
    public void getExtra(Activity activity) {

    }
}
