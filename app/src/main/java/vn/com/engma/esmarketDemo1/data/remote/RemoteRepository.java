package vn.com.engma.esmarketDemo1.data.remote;

import android.accounts.NetworkErrorException;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.plugins.RxJavaPlugins;
import okhttp3.RequestBody;
import retrofit2.Call;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.NewsModel;
import vn.com.engma.esmarketDemo1.data.remote.dto.User;
import vn.com.engma.esmarketDemo1.data.remote.service.StandardService;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;
import vn.com.engma.esmarketDemo1.utils.L;
import vn.com.engma.esmarketDemo1.utils.ParamsConstants;
import vn.com.engma.esmarketDemo1.utils.Settings;

import static vn.com.engma.esmarketDemo1.data.remote.ServiceError.NETWORK_ERROR;
import static vn.com.engma.esmarketDemo1.data.remote.ServiceError.SUCCESS_CODE;
import static vn.com.engma.esmarketDemo1.utils.Constants.ERROR_UNDEFINED;
import static vn.com.engma.esmarketDemo1.utils.NetworkUtils.isConnected;
import static vn.com.engma.esmarketDemo1.utils.ObjectUtil.isNull;

/**
 * Created by AhmedEltaher on 5/12/2016
 */

public class RemoteRepository implements RemoteSource {
    private ServiceGenerator serviceGenerator;
    private final String UNDELIVERABLE_EXCEPTION_TAG = "UNDELIVERABLE_EXCEPTION";

    @Inject
    public RemoteRepository(ServiceGenerator serviceGenerator) {
        this.serviceGenerator = serviceGenerator;
    }

    @Override
    public Single login(String username, String password) {
        RxJavaPlugins.setErrorHandler(throwable -> {
            Log.i(UNDELIVERABLE_EXCEPTION_TAG, throwable.getMessage());
            return;
        });

        Single<User.UserRespond> userRespondSingle = Single.create(singleOnSubscribe -> {
                    if (!isConnected(App.getContext())) {
                        Exception exception = new NetworkErrorException();
                        singleOnSubscribe.onError(exception);
                    } else {
                        try {
                            Map<String, String> params = new HashMap<>();
                            CommonUtils.sendIdPhone(params, App.getContext());
                            params.put(ParamsConstants.USERNAME, username);
                            params.put(ParamsConstants.PASSWORD, CommonUtils.hashPassword(password));
                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(params)).toString());
                            StandardService savistaService = serviceGenerator.createService(StandardService.class, Settings.URL_SERVER);
                            ServiceResponse serviceResponse = processCall(savistaService.login(body), false);
                            if (serviceResponse.getCode() == SUCCESS_CODE) {
                                User.UserRespond userRespond = (User.UserRespond) serviceResponse.getData();
                                singleOnSubscribe.onSuccess(userRespond);
                            } else {
                                Throwable throwable = new NetworkErrorException();
                                singleOnSubscribe.onError(throwable);
                            }
                        } catch (Exception e) {
                            singleOnSubscribe.onError(e);
                        }
                    }
                }
        );
        return userRespondSingle;
    }

    @Override
    public Single getRecordsPerCategory(Map<String, String> map) {
        RxJavaPlugins.setErrorHandler(throwable -> {
            Log.i(UNDELIVERABLE_EXCEPTION_TAG, throwable.getMessage());
            return;
        });

        Single<ModelCagetory> userRespondSingle = Single.create(singleOnSubscribe -> {
                    if (!isConnected(App.getContext())) {
                        Exception exception = new NetworkErrorException();
                        singleOnSubscribe.onError(exception);
                    } else {
                        try {
                            Map<String, String> params = new HashMap<>();
                            CommonUtils.sendIdPhone(params, App.getContext());
                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(params)).toString());
                            StandardService savistaService = serviceGenerator.createService(StandardService.class, Settings.URL_SERVER);
                            ServiceResponse serviceResponse = processCall(savistaService.get_records_per_category(body), false);
                            if (serviceResponse.getCode() == SUCCESS_CODE) {
                                ModelCagetory modelCagetoryRespond = (ModelCagetory) serviceResponse.getData();
                                singleOnSubscribe.onSuccess(modelCagetoryRespond);
                            } else {
                                Throwable throwable = new NetworkErrorException();
                                singleOnSubscribe.onError(throwable);
                            }
                        } catch (Exception e) {
                            singleOnSubscribe.onError(e);
                        }
                    }
                }
        );
        return userRespondSingle;

    }

    @Override
    public Single getNews() {
        RxJavaPlugins.setErrorHandler(throwable -> {
            Log.i(UNDELIVERABLE_EXCEPTION_TAG, throwable.getMessage());
            return;
        });
        Single<NewsModel> newsModelSingle = Single.create(singleOnSubscribe -> {
                    if (!isConnected(App.getContext())) {
                        Exception exception = new NetworkErrorException();
                        singleOnSubscribe.onError(exception);
                    } else {
                        try {
                            StandardService savistaService = serviceGenerator.createService(StandardService.class, Settings.URL_SERVER);
                            ServiceResponse serviceResponse = processCall(savistaService.fetchNews(), false);
                            if (serviceResponse.getCode() == SUCCESS_CODE) {
                                NewsModel newsModel = (NewsModel) serviceResponse.getData();
                                singleOnSubscribe.onSuccess(newsModel);
                            } else {
                                Throwable throwable = new NetworkErrorException();
                                singleOnSubscribe.onError(throwable);
                            }
                        } catch (Exception e) {
                            singleOnSubscribe.onError(e);
                        }
                    }
                }
        );
        return newsModelSingle;
    }

    @NonNull
    private ServiceResponse processCall(Call call, boolean isVoid) {
        if (!isConnected(App.getContext())) {
            return new ServiceResponse(new ServiceError());
        }
        try {
            retrofit2.Response response = call.execute();
            Gson gson = new Gson();
            L.json(NewsModel.class.getName(), gson.toJson(response.body()));
            if (isNull(response)) {
                return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
            }
            int responseCode = response.code();
            if (response.isSuccessful()) {
                return new ServiceResponse(responseCode, isVoid ? null : response.body());
            } else {
                ServiceError ServiceError;
                ServiceError = new ServiceError(response.message(), responseCode);
                return new ServiceResponse(ServiceError);
            }
        } catch (IOException e) {
            return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
        }
    }
}
