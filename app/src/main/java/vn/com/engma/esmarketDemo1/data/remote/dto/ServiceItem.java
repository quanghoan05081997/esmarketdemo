
package vn.com.engma.esmarketDemo1.data.remote.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ServiceItem implements Parcelable {

    @SerializedName("company_name")
    private String company_name;

    @SerializedName("service_name")
    private String service_name;

    @SerializedName("rate_num")
    private String rate_num;

    @SerializedName("price")
    private String price;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.company_name);
        dest.writeString(this.service_name);
        dest.writeString(this.rate_num);
        dest.writeString(this.price);
    }

    public ServiceItem() {
    }

    protected ServiceItem(Parcel in) {
        this.company_name = in.readString();
        this.service_name = in.readString();
        this.rate_num = in.readString();
        this.price = in.readString();
    }

    public static final Creator<ServiceItem> CREATOR = new Creator<ServiceItem>() {
        @Override
        public ServiceItem createFromParcel(Parcel source) {
            return new ServiceItem(source);
        }

        @Override
        public ServiceItem[] newArray(int size) {
            return new ServiceItem[size];
        }
    };

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getRate_num() {
        return rate_num;
    }

    public void setRate_num(String rate_num) {
        this.rate_num = rate_num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
