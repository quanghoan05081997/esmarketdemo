package vn.com.engma.esmarketDemo1.ui.base.listeners;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface ProgressView {
    void showProgress(String message);

    void hideProgress();
}
