package vn.com.engma.esmarketDemo1.data.remote.dto.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nhanmai on 1/15/18.
 */

public class EObject implements Cloneable {
    @Expose
    @SerializedName("_id")
    String _id;


    public String toJsonString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();      //ignore keys which have no @Expose
        return gson.toJson(this);
    }

    public static EObject fromJson(String jsonString, Class<? extends EObject> cls) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .create();      //ignore keys which have no @Expose
        return gson.fromJson(jsonString, cls);
    }

    public static EObject fromJson(String jsonString, Class<? extends EObject> cls, boolean boolFormat) {
        if (boolFormat) {
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                    .create();      //ignore keys which have no @Expose
            return gson.fromJson(jsonString, cls);
        } else {
            return fromJson(jsonString, cls);
        }

    }


    public static Gson getGson() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();      //ignore keys which have no @Expose
        return gson;
    }

    public EObject() {
    }

    public EObject(String _id) {
        this._id = _id;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    @Override
    public EObject clone() {
        try {
            return (EObject) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}
