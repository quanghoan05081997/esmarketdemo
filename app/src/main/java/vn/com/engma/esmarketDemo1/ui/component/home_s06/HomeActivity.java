package vn.com.engma.esmarketDemo1.ui.component.home_s06;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.ui.base.BaseActivity;
import vn.com.engma.esmarketDemo1.ui.component.fragment.account_fragment.AccountFragmentActivity;
import vn.com.engma.esmarketDemo1.ui.component.fragment.home_fragment.HomeFragmentActivity;
import vn.com.engma.esmarketDemo1.ui.component.fragment.news_fragment.NewsFragmentActivity;
import vn.com.engma.esmarketDemo1.ui.component.fragment.shoppingcart_fragment.ShoppingcartFragmentActivity;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;

import static vn.com.engma.esmarketDemo1.App.getContext;

/**
 * Created by tuannguyen on 13/12/17.
 */

public class HomeActivity extends BaseActivity implements HomeContract.View {

    //ArrayList<Product> arrayList;

    @Inject
    public HomePresenter presenter;

    @Nullable
    @BindView(R.id.lvCategory)
    ListView listViewCategory;

    @Nullable
    @BindView(R.id.viewFlipper)
    ViewFlipper Viewflipper;

    @Nullable
    @BindView(R.id.imgTrangchu)
    ImageView imgTrangchu;

    @Nullable
    @BindView(R.id.imgNews)
    ImageView imgNews;

    @Nullable
    @BindView(R.id.imgGiohang)
    ImageView imgGiohang;

    @Nullable
    @BindView(R.id.imgAccount)
    ImageView imgAccount;

    @Nullable
    @BindView(R.id.tvTrangchu)
    TextView tvTrangchu;
    @Nullable
    @BindView(R.id.tvNews)
    TextView tvNews;
    @Nullable
    @BindView(R.id.tvGiohang)
    TextView tvGiohang;

    @Nullable
    @BindView(R.id.tvAccount)
    TextView tvAccount;

    @Nullable
    @BindView(R.id.llTrangchu)
    LinearLayout llHome;

    @Nullable
    @BindView(R.id.llNews)
    LinearLayout llNews;

    @Nullable
    @BindView(R.id.llGiohang)
    LinearLayout llGiohang;

    @Nullable
    @BindView(R.id.llAccount)
    LinearLayout llAccount;




    SharedPreferences settingSharedPreferences;

    @Override
    public int getLayoutId() {

        return R.layout.home_activity_s06;
    }

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(HomeActivity.this);


    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        CommonUtils.hideKeyboard(HomeActivity.this);

    }

    @OnClick({R.id.llTrangchu,R.id.llNews,R.id.llGiohang,R.id.llAccount})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llTrangchu:{
                navigativHomesScreen();
                break;
            }
            case R.id.llNews:{
                navigativeNewsScreen();
                break;
            }
            case R.id.llGiohang:{
                navigativeGiohangScreen();
                break;
            }
            case R.id.llAccount:{
                navigativeAccountScreen();
                break;
            }

        }
    }

    @Override
    public void navigateToHomeScreen() {

    }

    @Override
    public void showDialog() {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void showErrToast(int errMessage) {
        CommonUtils.showAppToast(HomeActivity.this, errMessage);
    }

    @Override
    public void showErrorMessage(int errMessage) {

    }

    @Override
    public void showPopupMenu() {

    }

    @Override
    public void showToast(int errMessage) {

    }

    @Override
    public void showPopupMenu(View view) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    public void restartTabBot() {
        imgTrangchu.setImageResource(R.drawable.home_black);
        imgNews.setImageResource(R.drawable.news);
        imgGiohang.setImageResource(R.drawable.shopping_cart);
        imgAccount.setImageResource(R.drawable.user);
        tvTrangchu.setTextColor(getResources().getColor(R.color.colorNotPick));
        tvNews.setTextColor(getResources().getColor(R.color.colorNotPick));
        tvGiohang.setTextColor(getResources().getColor(R.color.colorNotPick));
        tvAccount.setTextColor(getResources().getColor(R.color.colorNotPick));
    }

    @Override
    public void navigativeNewsScreen() {

        restartTabBot();
        tvNews.setTextColor(getResources().getColor(R.color.colorRed));
        imgNews.setImageResource(R.drawable.news_red);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_home, new NewsFragmentActivity(), this.toString())
                .commit();
    }

    @Override
    public void navigativeGiohangScreen() {
        restartTabBot();
        tvGiohang.setTextColor(getResources().getColor(R.color.colorRed));
        imgGiohang.setImageResource(R.drawable.shopping_cart_red);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_home, new ShoppingcartFragmentActivity(), this.toString())
                .commit();

    }

    @Override
    public void navigativeAccountScreen() {
        restartTabBot();
        tvAccount.setTextColor(getResources().getColor(R.color.colorRed));
        imgAccount.setImageResource(R.drawable.user_red);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_home, new AccountFragmentActivity(), this.toString())
                .commit();

    }

    @Override
    public void navigativHomesScreen() {
        restartTabBot();
        tvTrangchu.setTextColor(getResources().getColor(R.color.colorRed));
        imgTrangchu.setImageResource(R.drawable.home);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_home, new HomeFragmentActivity(), this.toString())
                .commit();
    }

}
