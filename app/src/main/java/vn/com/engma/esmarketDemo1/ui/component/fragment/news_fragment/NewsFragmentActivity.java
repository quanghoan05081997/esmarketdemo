package vn.com.engma.esmarketDemo1.ui.component.fragment.news_fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.ui.base.BaseActivity;
import vn.com.engma.esmarketDemo1.ui.base.BaseFragment;

import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomeContract;
import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomePresenter;

import vn.com.engma.esmarketDemo1.utils.CommonUtils;

import static com.facebook.FacebookSdk.getApplicationContext;
import static vn.com.engma.esmarketDemo1.App.getContext;

/**
 * Created by tuannguyen on 13/12/17.
 */

public class NewsFragmentActivity extends BaseFragment implements NewsFragmentContract.View {



    @Inject
    NewsFragmentPresenter presenter;



    @Override
    public int getLayoutId() {

        return R.layout.news_fragment_s06_layout;
    }

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(NewsFragmentActivity.this);
        
        
    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        CommonUtils.hideKeyboard(activity);
    }


    @OnClick({})
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }


    @Override
    public void showErrorMessage(int errMessage) {

    }

    @Override
    public void showPopupMenu() {

    }


}
