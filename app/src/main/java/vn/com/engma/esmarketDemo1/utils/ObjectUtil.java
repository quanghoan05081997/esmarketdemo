package vn.com.engma.esmarketDemo1.utils;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Created by AhmedEltaher on 5/12/2016.
 */

public class ObjectUtil {
    public static boolean isEmptyStr(String string) {
        return TextUtils.isEmpty(string);
    }

    public static boolean isEmptyBytes(byte[] bytes) {
        return bytes == null || bytes.length == 0;
    }

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmptyList(List list) {
        return list == null || list.isEmpty();
    }

    public static boolean isEmptyMap(HashMap hashMap) {
        return hashMap == null || hashMap.isEmpty();
    }
}
