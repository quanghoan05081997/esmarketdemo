package vn.com.engma.esmarketDemo1.data.remote.dto.base;

/**
 * Created by nhanmai on 1/18/18.
 */

public class ERespond {

    public static final int OK = 200;
    public static final int SERVER_ERROR = 500;
    public static final int SERVER_BAD_REQUEST = 400;
    public static final int SERVER_NOT_FOUND = 404;


    int status;
    String message;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
