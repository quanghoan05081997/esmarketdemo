package vn.com.engma.esmarketDemo1.data.remote.service;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.data.remote.dto.NewsModel;
import vn.com.engma.esmarketDemo1.data.remote.dto.User;

/**
 * Created by AhmedEltaher on 5/12/2016
 */

public interface StandardService {
    @GET("topstories/v2/home.json")
    Call<NewsModel> fetchNews();

    @POST("_api/user/login")
    Call<User.UserRespond> login(@Body RequestBody params);

    @POST("_api/record/get_records_per_category")
    Call<ModelCagetory> get_records_per_category(@Body RequestBody params);


}
