package vn.com.engma.esmarketDemo1.ui.component.login_esmarket_s01;


import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import vn.com.engma.esmarketDemo1.App;
import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.social.GooglePlusAPI;
import vn.com.engma.esmarketDemo1.ui.base.BaseActivity;
import vn.com.engma.esmarketDemo1.ui.component.home_s06.HomeActivity;
import vn.com.engma.esmarketDemo1.ui.component.main.MainActivity;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;
import vn.com.engma.esmarketDemo1.utils.NetworkUtils;

/**
 * Created by tuannguyen on 13/12/17.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View {

    @Inject
    LoginPresenter presenter;
    @Nullable
    @BindView(R.id.tvForgetPassword)
    TextView tvForgetPassword;
    @Nullable
    @BindView(R.id.tvRegister)
    TextView tvRegister;

    @Nullable
    @BindView(R.id.tvErrorMessage)
    TextView tvErrorMessage;

    @Nullable
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @Nullable
    @BindView(R.id.etUsername)
    EditText etUsername;

    @Nullable
    @BindView(R.id.etPassword)
    EditText etPassword;

    SharedPreferences settingSharedPreferences;

    @Override
    public int getLayoutId() {
        settingSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return R.layout.login_activity_s06;
    }

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(LoginActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        CommonUtils.hideKeyboard(LoginActivity.this);
    }


    @OnClick({R.id.tvForgetPassword, R.id.btnLogin, R.id.tvRegister, R.id.btnLoginFB})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvForgetPassword:
                if (!NetworkUtils.isConnected(LoginActivity.this)) {
                    showErrToast(R.string.err_no_connected);
                    return;
                }
                presenter.navigateToForgotPasswordScreen();
                break;
            case R.id.tvRegister:
                if (!NetworkUtils.isConnected(LoginActivity.this)) {
                    showErrToast(R.string.err_no_connected);
                    return;
                }
                navigateToRegisterScreen();
                break;
            case R.id.btnLogin:
                if (!NetworkUtils.isConnected(LoginActivity.this)) {
                    showErrToast(R.string.err_no_connected);
                    return;
                }
                String username = etUsername.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                presenter.sendRequestLogin(username, password);
                break;
            case R.id.btnLoginFB:
                if (!NetworkUtils.isConnected(LoginActivity.this)) {
                    showErrToast(R.string.err_no_connected);
                    return;
                }
                presenter.sendRequestLoginFB();
                break;
        }
    }


    @Override
    public void navigateToMainScreen() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToForgotScreen() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToRegisterScreen() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

//    public void navigateToLoginFB(){
//        Intent intent = new Intent(LoginActivity.this,Home_s06_Activity.class);
//        startActivity(intent);
//        finish();
//    }

    @Override
    public void navigateToHomeScreen() {

    }

    @Override
    public void showDialog() {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void showErrToast(int errMessage) {
        CommonUtils.showAppToast(LoginActivity.this, errMessage);
    }

    @Override
    public void showErrorMessage(int errMessage) {
        try {
            tvErrorMessage.setVisibility(View.VISIBLE);
            tvErrorMessage.setText(errMessage);
        } catch (Exception e) {
        }
    }

    @Override
    public void showPopupMenu() {

    }

    @Override
    public void showToast(int errMessage) {

    }

    @Override
    public void showPopupMenu(View view) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GooglePlusAPI.RC_SIGN_IN:
                presenter.setLoginSocialResult(data);
                break;
        }
    }
}
