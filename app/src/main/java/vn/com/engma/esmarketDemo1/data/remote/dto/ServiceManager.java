
package vn.com.engma.esmarketDemo1.data.remote.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ServiceManager implements Parcelable {

    @SerializedName("trading_code")
    private String trading_code;

    @SerializedName("date")
    private String date;

    @SerializedName("service_name")
    private String service_name;

    @SerializedName("quality")
    private String quality;

    @SerializedName("executor")
    private String executor;

    @SerializedName("note")
    private String note;

    @SerializedName("price")
    private String price;

    @SerializedName("status")
    private String status;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.trading_code);
        dest.writeString(this.date);
        dest.writeString(this.service_name);
        dest.writeString(this.quality);
        dest.writeString(this.executor);
        dest.writeString(this.note);
        dest.writeString(this.price);
        dest.writeString(this.status);
    }

    public ServiceManager() {
    }

    protected ServiceManager(Parcel in) {
        this.trading_code = in.readString();
        this.date = in.readString();
        this.service_name = in.readString();
        this.quality = in.readString();
        this.executor = in.readString();
        this.note = in.readString();
        this.price = in.readString();
        this.status = in.readString();
    }

    public static final Creator<ServiceManager> CREATOR = new Creator<ServiceManager>() {
        @Override
        public ServiceManager createFromParcel(Parcel source) {
            return new ServiceManager(source);
        }

        @Override
        public ServiceManager[] newArray(int size) {
            return new ServiceManager[size];
        }
    };

    public String getTrading_code() {
        return trading_code;
    }

    public void setTrading_code(String trading_code) {
        this.trading_code = trading_code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public static Creator<ServiceManager> getCREATOR() {
        return CREATOR;
    }
}
