package vn.com.engma.esmarketDemo1.data.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import vn.com.engma.esmarketDemo1.data.remote.dto.base.EObject;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;

/**
 * Created by nhanmai on 1/15/18.
 */

public class CagetoryItem extends EObject {
    public static class UserRespond extends ERespond {
        CagetoryItem data;

        public CagetoryItem getData() {
            return data;
        }
    }

    @Expose
    @SerializedName("name")
    String name;

    @SerializedName("data_record")
    private List<ProductItem> productItemList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductItem> getProductItemList() {
        return productItemList;
    }

    public void setProductItemList(List<ProductItem> productItemList) {
        this.productItemList = productItemList;
    }
}
