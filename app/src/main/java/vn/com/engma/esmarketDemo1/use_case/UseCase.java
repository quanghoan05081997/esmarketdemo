package vn.com.engma.esmarketDemo1.use_case;


import java.util.Map;

import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseCallback;

public interface UseCase {
    void login(String username, String password, final BaseCallback callback);

    void requestCategory (Map<String,String> map,final BaseCallback callback );
}
