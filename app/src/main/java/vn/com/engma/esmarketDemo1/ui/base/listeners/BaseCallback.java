package vn.com.engma.esmarketDemo1.ui.base.listeners;


import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface BaseCallback {
    void onSuccess(ERespond newsModel);

    void onFail(Throwable error);
}
