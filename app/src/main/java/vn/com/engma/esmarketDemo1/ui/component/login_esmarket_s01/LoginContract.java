package vn.com.engma.esmarketDemo1.ui.component.login_esmarket_s01;

import android.content.Intent;

import java.util.HashMap;

import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;


/**
 * Created by tuannguyen on 13/12/17.
 */

public interface LoginContract {
    interface View extends BaseView {
        void navigateToMainScreen();

        void navigateToForgotScreen();

        void navigateToRegisterScreen();

        void showErrToast(int errMessage);


    }

    interface Presenter {
        void navigateToForgotPasswordScreen();

        void navigateToRegisterScreen();

        void sendRequestLogin(String username, String password);

        void sendRequestLoginGG();

        void sendRequestLoginFB();

        void setLoginSocialResult(Intent intent);

        void onActivityResult(int requestCode, int resultCode, Intent data);


        HashMap<String, Integer> isValidUserName(String username);

        HashMap<String, Integer> isValidPassword(String username);
    }
}
