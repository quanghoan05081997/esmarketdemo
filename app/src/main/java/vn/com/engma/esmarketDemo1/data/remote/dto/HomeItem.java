
package vn.com.engma.esmarketDemo1.data.remote.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class HomeItem implements Parcelable {

    @SerializedName("item_src")
    private String item_src;

    @SerializedName("item_name")
    private String item_name;

    @SerializedName("item_code")
    private String item_code;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.item_src);
        dest.writeString(this.item_name);
        dest.writeString(this.item_code);
    }

    public HomeItem() {
    }

    protected HomeItem(Parcel in) {
        this.item_src = in.readString();
        this.item_name = in.readString();
        this.item_code = in.readString();
    }

    public static final Creator<HomeItem> CREATOR = new Creator<HomeItem>() {
        @Override
        public HomeItem createFromParcel(Parcel source) {
            return new HomeItem(source);
        }

        @Override
        public HomeItem[] newArray(int size) {
            return new HomeItem[size];
        }
    };

    public String getItem_src() {
        return item_src;
    }

    public void setItem_src(String item_src) {
        this.item_src = item_src;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public static Creator<HomeItem> getCREATOR() {
        return CREATOR;
    }
}
