package vn.com.engma.esmarketDemo1.ui.component.login_esmarket_s01;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.HashMap;

import javax.inject.Inject;

import vn.com.engma.esmarketDemo1.R;
import vn.com.engma.esmarketDemo1.data.remote.dto.User;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.ERespond;
import vn.com.engma.esmarketDemo1.data.remote.dto.base.HttpMessage;
import vn.com.engma.esmarketDemo1.data.social.FacebookAPI;
import vn.com.engma.esmarketDemo1.data.social.GooglePlusAPI;
import vn.com.engma.esmarketDemo1.data.social.LoginSocialResult;
import vn.com.engma.esmarketDemo1.data.social.LoginType;
import vn.com.engma.esmarketDemo1.ui.base.Presenter;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseCallback;
import vn.com.engma.esmarketDemo1.use_case.UserUseCase;
import vn.com.engma.esmarketDemo1.utils.CommonUtils;
import vn.com.engma.esmarketDemo1.utils.Constants;
import vn.com.engma.esmarketDemo1.utils.L;


/**
 * Created by TuanNguyen on 15/12/2017
 */

public class LoginPresenter extends Presenter<LoginContract.View> implements LoginContract.Presenter, LoginSocialResult {

    private UserUseCase useCase;
    private GooglePlusAPI googlePlusAPI;
    private FacebookAPI facebookAPI;

    @Inject
    public LoginPresenter(UserUseCase demoUseCase) {
        this.useCase = demoUseCase;

    }

//    @Inject
//    public LoginPresenter() {
//
//    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
        this.googlePlusAPI = new GooglePlusAPI(activity, this);
        this.facebookAPI = new FacebookAPI(activity, this);
    }
    @Override
    public void getExtra(Activity activity) {

    }

    @Override
    public void navigateToForgotPasswordScreen() {
        getView().navigateToForgotScreen();
    }

    @Override
    public void navigateToRegisterScreen() {
        getView().navigateToRegisterScreen();
    }

    @Override
    public void sendRequestLogin(String username, String password) {
        //send request login
        if (isValidPassword(password).get(Constants.PARAM_RESULT_CODE) == 1 && isValidUserName(username).get(Constants.PARAM_RESULT_CODE) == 1) {
            useCase.login(username, password, new BaseCallback() {
                @Override
                public void onSuccess(ERespond respondUser) {
                    handleResultLogin((User.UserRespond) respondUser);
                }

                @Override
                public void onFail(Throwable error) {
                    getView().showErrorMessage(R.string.login_travel_s01_err_try_again);
                    L.e("Nhan", error.toString());
                }
            });


        } else {
            if (isValidUserName(username).get(Constants.PARAM_RESULT_CODE) == 0) {
                getView().showErrToast(isValidUserName(username).get(Constants.PARAM_ERROR_MESSAGE));
            } else {
                getView().showErrToast(isValidPassword(password).get(Constants.PARAM_ERROR_MESSAGE));
            }
        }
    }

    @Override
    public void sendRequestLoginFB() {
        facebookAPI.signIn();
    }

    @Override
    public void sendRequestLoginGG() {
        googlePlusAPI.signIn();
    }

    @Override
    public void setLoginSocialResult(Intent intent) {
        googlePlusAPI.handleSignInResult(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookAPI.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public HashMap<String, Integer> isValidUserName(String username) {
        return CommonUtils.isUserNameValid(username);
    }

    @Override
    public HashMap<String, Integer> isValidPassword(String password) {
        return CommonUtils.isValidPassword(password);
    }

    @Override
    public void loginResult(LoginType loginType, String socialId, String email, String birthday, String first_name, String last_name, String gender, String avatar) {

        useCase.login_social(loginType, socialId, email, birthday, first_name, last_name, gender, avatar, new BaseCallback() {
            @Override
            public void onSuccess(ERespond respondUser) {
                handleResultLogin((User.UserRespond) respondUser);
            }

            @Override
            public void onFail(Throwable error) {
                getView().showErrorMessage(R.string.login_travel_s02_err_try_again);
                L.e("Hoan", error.toString());
            }
        });
    }

    @Override
    public void errorResult(LoginType loginType, Throwable error) {

    }

    private void handleResultLogin(User.UserRespond respondUser) {
        if (respondUser.getStatus() == ERespond.OK) {
            //@// TODO: 1/18/18 save login user
            getView().navigateToMainScreen();
        } else {
            switch (respondUser.getMessage()) {
                case HttpMessage.LOGIN_FAILED:
                case HttpMessage.NOT_FOUND:
                    getView().showErrorMessage(R.string.login_travel_s01_err_account_not_found);
                    break;
                case HttpMessage.BANNED:
                    getView().showErrorMessage(R.string.login_travel_s01_err_account_banned);
                    break;
                case HttpMessage.NON_ACTIVATE:
                    getView().showErrorMessage(R.string.login_travel_s01_err_non_active);
                    break;
                default:
                    getView().showErrorMessage(R.string.login_travel_s01_err_try_again);
                    break;
            }
        }
    }
}
