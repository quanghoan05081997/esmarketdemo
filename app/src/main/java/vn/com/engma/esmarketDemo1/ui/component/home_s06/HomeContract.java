package vn.com.engma.esmarketDemo1.ui.component.home_s06;

import android.content.Intent;

import java.util.HashMap;
import java.util.List;

import vn.com.engma.esmarketDemo1.data.remote.dto.CagetoryItem;
import vn.com.engma.esmarketDemo1.data.remote.dto.ModelCagetory;
import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;


/**
 * Created by tuannguyen on 13/12/17.
 */

public interface HomeContract {
    interface View extends BaseView {

//        void displayProduct (ModelCagetory modelCagetory  );

        void restartTabBot();


        void navigativeNewsScreen();

        void navigativeGiohangScreen();

        void navigativeAccountScreen();

        void navigativHomesScreen();
    }

    interface Presenter {
//        void getCategory(HashMap<String,String> params);

        void onActivityResult(int requestCode, int resultCode, Intent data);
    }
}
