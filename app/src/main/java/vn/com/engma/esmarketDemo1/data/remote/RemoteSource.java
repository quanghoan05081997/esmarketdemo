package vn.com.engma.esmarketDemo1.data.remote;

import java.util.Map;

import io.reactivex.Single;

/**
 * Created by ahmedeltaher on 3/23/17.
 */

interface RemoteSource {

    Single login(String username, String password);

    Single getRecordsPerCategory (Map<String,String> map);
    Single getNews();


}
