package vn.com.engma.esmarketDemo1.ui.component.fragment.shoppingcart_fragment;

import vn.com.engma.esmarketDemo1.ui.base.listeners.BaseView;


/**
 * Created by tuannguyen on 13/12/17.
 */

public interface ShoppingcartFragmentContract {
    interface View extends BaseView {

    }

    interface Presenter {
    }
}
